<?php

namespace App\Http\ApiV1\Modules\Suggests\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class SearchSuggestsRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'filter.query' => ['required'],
        ];
    }
}
