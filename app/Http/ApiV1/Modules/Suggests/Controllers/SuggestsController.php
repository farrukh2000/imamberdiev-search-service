<?php

namespace App\Http\ApiV1\Modules\Suggests\Controllers;

use Illuminate\Http\Request;
use App\Http\ApiV1\Modules\Suggests\Resources\SuggestsResource;
use App\Domain\Suggests\Actions\SearchSuggestsAction;
use App\Http\ApiV1\Modules\Suggests\Requests\SearchSuggestsRequest;

class SuggestsController
{
    public function search(SearchSuggestsRequest $request, SearchSuggestsAction $action) 
    {
        return SuggestsResource::collection($action->execute($request->validated()));
    }
}
