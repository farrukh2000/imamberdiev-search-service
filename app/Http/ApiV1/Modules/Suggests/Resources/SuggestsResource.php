<?php

namespace App\Http\ApiV1\Modules\Suggests\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin App\Domain\Suggests\Dtos\SuggestsDto
 */
class SuggestsResource extends BaseJsonResource
{
    public function toArray($request) : array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'picture_src' => $this->picture_src,
            'user_id' => $this->user_id,
        ];
    }
}
