<?php

namespace App\Domain\Suggests\Actions;

use Elasticsearch\ClientBuilder;
use App\Domain\Suggests\Dtos\SuggestsDto;

class SearchSuggestsAction
{
    public function execute(array $fields)
    {
        $client = ClientBuilder::create()->setHosts([config('elasticsearch.host')])->build();

        $params = [
            'index' => 'posts',
            'body'  => [
                'query' => [
                    'multi_match' => [
                        'query' => $fields['filter']['query'],
                        'fields' => [
                            'title^2',
                            'description'
                        ]
                    ]
                ]
            ]
        ];
              
        $searchResult = $client->search($params);

        $data = [];
        foreach($searchResult['hits']['hits'] as $item) {
            $item['_source']['id'] = $item['_id'];
            $data[] = new SuggestsDto($item['_source']);
        }

        return $data;
    }
}
