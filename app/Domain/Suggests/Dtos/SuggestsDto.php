<?php

namespace  App\Domain\Suggests\Dtos;

use Illuminate\Support\Fluent;


/**
 * SuggestsDto
 * @package App\Domain\Suggests\Dtos
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $picture_src
 * @property integer $user_id
 */
class SuggestsDto extends Fluent
{
}